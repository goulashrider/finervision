<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::get ( '/', [
		'as' => 'home',
		'uses' => 'DataController@index'
] );

Route::get ( '/data', [
		'as' => 'data.list',
		'uses' => 'DataController@records'
]);

Route::post ( '/data', [
		'as' => 'data.add',
		'uses' => 'DataController@add'
]);

Route::delete ( '/data/{id}', [
		'as' => 'data.remove',
		'uses' => 'DataController@remove'
]);

Route::get ('/thank-you', [
		'as' => 'thankyou',
		'uses' => 'DataController@thankyou'
]);