<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>Finer Vision - tech project</title>

<!--  jQuery  -->

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>


<!-- Form validator -->

<script
	src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<!-- Styles -->
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>

	<div class="content">
		<div class="panel">

			<div>
				<div class="section-head">
					<div class="section-head-text">Step 1: Your details</div>
				</div>
			</div>
			<div class="section" id="section1">
				<form id="form-section1" action="">
					<div class="section-content">
						<div class="input-fields">
							<div class="input-group" id="first-name">
								<label for="firstname">First name</label> <input
									class="input-field" type="text" name="firstname" required>

							</div>
							<div class="input-group" id="last-name">
								<label for="lastname">Surname</label> <input class="input-field"
									type="text" name="lastname" required>

							</div>
							<div class="clearfix"></div>
							<div id="email">
								<label for="email">Email Address:</label> <input
									class="input-field" type="email" name="email" required>

							</div>
						</div>
					</div>


					<div>
						<input class="button-next" id="step1" type="submit" value="Next >">
					</div>
				</form>
			</div>
			<div>
				<div class="section-head">
					<div class="section-head-text">Step 2: More comments</div>
				</div>
			</div>
			<div class="section" id="section2">
				<form id="form-section2" action="">

					<div class="section-content">
						<div class="input-fields">
							<div class="input-group" id="phone">
								<label for="phone">Telephone number</label> <input
									class="input-field" type="text" name="phone" required>

							</div>
							<div class="input-group" id="gender">
								<label for="gender">Gender</label> <select class="input-field"
									name="gender" required>
									<option>Select gender</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>

							</div>
							<div class="clearfix"></div>
							<div id="dob">
								<label for="dob">Date of birth</label> <input
									class="input-field" type="text" name="dd" required> <input
									class="input-field" type="text" name="mm" required> <input
									class="input-field" type="text" name="yy" required>

							</div>
						</div>
					</div>

					<div>
						<input type="submit" class="button-next" id="step2" value="Next >">
					</div>
				</form>
			</div>
			<div class="section-head">
				<div class="section-head-text">Step 3: Final comments</div>
			</div>
			<div class="section" id="section3">
				<form id="form-section3" action="">
					<div></div>
					<div class="section-content">
						<div class="input-fields">
							<div class="input-group" id="comments">
								<label for="comments">Comments</label>
								<textarea class="comments" name="comments" required></textarea>
							</div>

						</div>
					</div>
					<div class="clearfix"></div>
					<div>
						<input type="submit" class="button-next" id="step3" value="Next >">
						{{ csrf_field() }}
					</div>
				</form>
			</div>

		</div>
	</div>
	<script type="text/javascript" ><?php echo "var url = '"; ?>{{ route('data.add') }}<?php echo "';"; ?></script>
	<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
