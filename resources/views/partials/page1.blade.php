@extends('layouts.base')

@section('title', 'Form') 



@section('content')

{{ Form::model($data, ['route' => ['data.add', $data], 'id' => 'data-form']) }}

@include('partials.section1')

@include('partials.section2')

@include('partials.section3')

	
{{ Form::close() }}

@endsection
