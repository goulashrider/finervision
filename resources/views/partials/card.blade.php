
<div class="card">
	<div class="head">{{ $d->firstname }} {{ $d->surename }}</div>
	<div class="content">
		<div class="contact">
			@<a href="mailto:{{ $d->email }}">{{ $d->email }}</a>, t: {{ $d->phone }}
		</div>
		<div class="detail">{{ $d->gender }}, DOB: {{
			date('d-m-Y',strtotime($d->dob)) }}</div>
		<div class="comment">{{ $d->comment }}</div>
	</div>
	
	{{ Form::open( ['method' => 'DELETE', 'route' => ['data.remove', $d->id]] ) }}
    	{{ Form::hidden('id', $d->id) }}
    	{{ Form::submit('Delete', ['class' => 'button-delete']) }}
    {{ Form::close() }}
	
	
	
</div>