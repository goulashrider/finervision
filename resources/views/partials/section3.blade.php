


<div class="section last" id="section3">
	<div class="section-head">
		<div class="section-head-text">Step 3: Final comments</div>
	</div>
	<div class="section-content">
		<div class="input-group">
			{{ Form::label('comments', 'Comments') }}
			{{ Form::textarea('comments', '', [ 'id' => 'comments', 'class' => 'comments', 'placeholder' => 'your comments...', 'required' => 'true']) }}
		</div>
		<div class="button-holder">
			{{ Form::submit('Next >', ['class' => 'button-next', 'id' => 'form-submit']) }}
		</div>
	</div>
</div>

