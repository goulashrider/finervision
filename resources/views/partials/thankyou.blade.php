@extends('layouts.base') @section('title', 'Stored records')


@section('content')

<div class="results">
	<div class="section-head">
		<div class="section-head-text">Thank you! Your details saved</div>
	</div>
	<div class="section-content">

		@include('partials.card', ['d' => $data])

		<a  href="{{ route('data.list')	 }}"><button class="button-next" type="button">All results ></button></a>
		<a  href="{{ route('home')	 }}"><button class="button-next" type="button">Back to Home</button></a>
	</div>
	
</div>

@endsection
