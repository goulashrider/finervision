@extends('layouts.base') @section('title', 'Stored records')


@section('content')

<div class="results">
	<div class="section-head">
		<div class="section-head-text">Records in database</div>
	</div>
	<div class="section-content">

		<div>
			<a href="{{ route('home')	 }}"><button class="button-next"
					type="button">Home</button></a>
		</div>

		@foreach ($data as $d) 
		
			@include('partials.card', ['data' => $d])

		@endforeach
		<div>
			<a href="{{ route('home')	 }}"><button class="button-next"
					type="button">Home</button></a>
		</div>
	</div>
</div>

@endsection
