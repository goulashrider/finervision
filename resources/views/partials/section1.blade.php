


<div class="section" id="section1">
	<div class="section-head">
		<div class="section-head-text">Step 1: Your details</div>
	</div>
	<div class="section-content">
		<div class="input-group">
			{{ Form::label('firstname', 'First name') }}
			{{ Form::text('firstname', '', [ 'class' => 'input-field', 'placeholder' => 'first name...', 'required' => 'true']) }}
		</div>
		<div class="input-group">
			{{ Form::label('lastname', 'Surename') }}
			{{ Form::text('lastname', '', [ 'class' => 'input-field', 'placeholder' => 'last name...', 'required' => 'true']) }}
		</div>
		<div class="clearfix"></div>
		<div class="input-group">
			{{ Form::label('email', 'Email Address:') }}
			{{ Form::email('email', '', [ 'class' => 'input-field', 'placeholder' => 'email...', 'required' => 'true']) }}
		</div>
		<div class="button-holder">
			{{ Form::button('Next >', ['class' => 'button-next section-submit']) }}
		</div>
	</div>

</div>

