


<div class="section" id="section2">
	<div class="section-head">
		<div class="section-head-text">Step 2: More comments</div>
	</div>
	<div class="section-content">
		<div class="input-group">
			{{ Form::label('phone', 'Telephone number') }}
			{{ Form::text('phone', '', [ 'class' => 'input-field', 'placeholder' => 'format: 07777112222', 'required' => 'true']) }}
		</div>
		<div class="input-group">
			{{ Form::label('gender', 'Gender') }}
			{{ Form::select('gender', ['' => 'Select gender...', 'Male' => 'Male', 'Female' => 'Female'], null, ['id' => 'gender', 'class' => 'input-field', 'required' => 'true']) }}
		</div>
		<div class="clearfix"></div>
		<div id="dob">
			{{ Form::label('dob', 'Date of birth') }}
			{{ Form::text('dd', '', [ 'id' => 'dd', 'class' => 'input-field', 'placeholder' => 'dd', 'required' => 'true']) }}
			{{ Form::text('mm', '', [ 'id' => 'mm', 'class' => 'input-field', 'placeholder' => 'mm', 'required' => 'true']) }}
			{{ Form::text('yy', '', [ 'id' => 'yy', 'class' => 'input-field', 'placeholder' => 'yyyy', 'required' => 'true']) }}
		</div>
		<div class="button-holder">
			{{ Form::button('Next >', ['class' => 'button-next section-submit']) }}
		</div>
	</div>
</div>

