<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>Test Project - @yield('title')</title>

<!--  jQuery  -->

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Form validator -->

<script
	src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<!-- Styles -->
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>

	<div class="content">
		<div class="panel">@yield('content')</div>
	</div>

	<footer>
		<script type="text/javascript"><?php echo "var url = '"; ?>{{ route('data.add') }}<?php echo "';"; ?></script>
		<script type="text/javascript" src="js/custom.js"></script>
	</footer>
</body>
</html>
