/*
 * Global variables
 */

// Data for AJAX post
var dat = [];

/*
 * x Document functions
 */

$(document).ready(function() {

	// calling toggle object
	sectionToggle();

});



function sectionToggle() {

	// toggle other sections by clicking section header

	$('.section-head').click(function() {

		var header = new HeaderToggle(this);
		header.click();

	})

	// toggle next section by clicking button

	$('.button-next').click(function() {

		var input = new InputValidation(this);

		// if inputs are valid, then toggle section

		if (input.valid()) {

			var button = new HeaderToggle(this);
			button.btnClick();

		};
	})
}

/*
 * Form validation object
 */

function InputValidation(el) {

	// validator rules
	var validator = $("#data-form").validate({
		rules : {
			firstname : {
				required : true,
				minlength : 2
			},
			lastname : {
				required : true,
				minlength : 2
			},
			email : {
				required : true,
				email : true
			},
			phone : {
				required : true,
				regex : true
			},
			gender : {
				required : true,
			},
			dd : {
				required : true,
				number : true,
				minlength : 2,
				maxlength : 2
			},
			mm : {
				required : true,
				number : true,
				minlength : 2,
				maxlength : 2
			},
			yy : {
				required : true,
				number : true,
				minlength : 4,
				maxlength : 4
			},
			comments : {
				required : true,
			}
		},
		errorPlacement : function(error, element) {
			offset = element.offset();

			error.insertBefore(element)
			error.addClass('error-msg'); // add a class to the wrapper
			error.css('position', 'absolute');
			error.css('left', 150);
			error.css('top', 15);
		},
		wrapper : 'div',
		errorElement : 'div'
	});

	// custom regex for phone format validation

	$.validator
			.addMethod("regex", function(value, element) {
				return this.optional(element)
						|| /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/
								.test(value);
			},
					'Format: 07222555555 or	07222 555555 or (07222) 555555 or +44 7222 555 555');

	// var for input names
	var inputs = [];

	this.section = $(el).closest('.section');

	// store input field names
	this.collectNames = function() {

		$(this.section).find('input').each(function(key, val) {

			inputs.push(val.name);

		})
	}

	this.collectNames();

	this.validateField = function() {

		var valid = false;

		$(inputs).each(function(key, val) {

			console.log("#" + val + ': ' + validator.element("#" + val));

			if (validator.element("#" + val)) {

				// returning validation valid
				valid = true;
			} else {
				// returning validation false
				valid = false;
			}
			;
		})

		return valid;
	}

	this.valid = function() {

		return this.validateField();

	}

	this.console = function() {
		console.log("#" + val + ': ' + validator.element("#" + val));
	};

}

/*
 * Header toggle object
 */

function HeaderToggle(el) {

	this.section = $(el).closest('.section');
	this.content = $(this.section).find('.section-content');

	this.id = $(this.section).attr('id');

	this.isVisible = function() {

		if ($(this.content).css('display') == 'none') {
			return false;
		} else {
			return true;
		}

	};

	this.click = function() {

		if (this.isVisible()) {
			this.setNonVisible();
		} else {
			this.setVisible();
		}

	}

	this.btnClick = function() {

		this.console();

		if (this.isVisible()) {

			// show next section
			if ($(this.section).attr('id') != 'section3') {
				new HeaderToggle($(this.section).next()).setVisible();

				// hide active section
				this.setNonVisible();
			}
		}
	}

	this.setVisible = function() {
		this.content.slideDown();
	}

	this.setNonVisible = function() {
		this.content.slideUp();
	}

	this.console = function() {
		// console.log($(this.section).attr('id'));
		// console.log(this);
		// console.log(el + ' Element visibility: ' + this.element.visible);
	};

}

function addFormData(form) {
	// var form = $(form);
	var form_json = ConvertFormToJSON(form);

	dat.push(form);

	// console.log(JSON.stringify(dat));
}

// AJAX

/*
 * $('#form-submit').click( function(e) {
 * 
 * //add record var form_data = ConvertFormToJSON($('#data-form'));
 * 
 * console.log(JSON.stringify(form_data));
 * 
 * 
 * $.ajax({ type: "POST", url: url, //data: JSON.stringify(dat), // serializes
 * the form's elements. data: form_data, // serializes the form's elements.
 * datatype: 'json', success: function(response) {
 * console.log(JSON.stringify(form_data));
 * 
 * //redirect thank you page }, error: function(e) {
 * console.log(e.responseText); } });
 * 
 * e.preventDefault(); });
 */

function ConvertFormToJSON(form) {
	var array = jQuery(form).serializeArray();
	var json = {};

	jQuery.each(array, function() {
		json[this.name] = this.value || '';
	});

	return json;
}

function revealAll(button) {
	$(button).click(function(e) {

		$('.section').slideDown();

		e.preventDefault();
	});
}