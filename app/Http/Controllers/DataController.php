<?php

namespace FinerVision\Http\Controllers;

use Illuminate\Http\Request;
use FinerVision\Http\Requests\StoreDataRequest;
use FinerVision\Data;

use DateTime;
use Illuminate\Support\Facades\Redirect;

class DataController extends Controller
{
    public function index() {
    	
    	$data = null;
    	
    	return view('partials.page1', ['data' => $data]);
    	
    }
    
    public function add(StoreDataRequest $request) {
    	
    	$data = new Data;
    	
    	$data->firstname = $request->firstname;
    	$data->surename = $request->lastname;
    	$data->email = $request->email;
    	$data->phone = $request->phone;
    	$data->gender = $request->gender;
    	
    	$d = new DateTime($request->dd . '-' . $request->mm . '-' . $request->yy);
    	
    	$dob = $d; // DateTime format
    	$formatted_date = $d->format('Y-m-d'); // 2003-10-16 - only for testing
    	
    	$data->dob = $dob;
    	$data->comment = $request->comments;
    	
    	// store data into DB
	   	$data->save();
		
	   	// redirect to thank you
    	return Redirect::route('thankyou');
    }
    
    public function records() {
    	
    	$data = new Data;
    	
    	return view('partials.records', ['data' => $data::all()]);
    }
    
    public function remove(StoreDataRequest $request) {
    	
    	$record = $request->id;
    	
    	$data = new Data;
    	
    	$data::destroy($record);
    	
    	return Redirect::route('data.list');
    	
    }
    
    public function thankyou() {
   		
    	// get last record in DB
    	$data = Data::orderBy('created_at', 'desc')->first();
    	
    	return view('partials.thankyou', ['data' => $data]);
    }
}
