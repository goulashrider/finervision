<?php

namespace FinerVision;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
	protected $fillable = [
			'firstname', 'surename', 'email', 'phone', 'gender', 'dob', 'comment', 'password',
	];
	
	protected $hidden = [
			'password', 'remember_token',
	];
	
}
