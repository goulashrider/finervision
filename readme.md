# Project description #


## Used technology ##

* Laravel framework
* Eloquent database ORM
* Composer
* Form - model binding
* jQuery
* jQuery form validation
* SASS
* GIT

## Used tools ##

* Virtualbox LAMP stack running on Windows
* Putty SSH terminal
* Eclipse
* Sourcetree
* MySQL Workbench
* Postman API tool


## Project requirements ##

### Data entry form programming test ###

Task: Create a simple data entry form that stores the required data into a database table.

### Requirements: ###

* Must use HTML, CSS, PHP and JavaScript (preferably with jQuery)
* Must store data in a MySQL database
* Must validate data prior to submission (using a JavaScript plugin is ok)
* The entry form must be split into 3 separate sections, after each section is complete the section slides out and and the next section slides in
* There should be an adequate amount of comments (without being too bloated with comments)

### The fields required on the fields are: ###

* First name
* Last name
* E-mail address (must be validated to be a correct address)
* Telephone number (must be validated to be a number)
* Gender
* Date of birth (must be validated to be a correct date)
* Comments

Present the user with a form starting on section one, at the end of each section add a next button to slide to the next section. Once the form is complete present a thank-you page. Also create a page that displays all the entries in the database.

### Extra niceties: ###
* Use pretty CSS3 buttons with a gradient and rounded corners
* Use the html5 doctype
* Make the validation part of the design

### Design ###

See [design.psd](https://bitbucket.org/goulashrider/finervision/src/d2554f37bc913e4584ed910db10d9cafb755eee0/design.psd?at=master)