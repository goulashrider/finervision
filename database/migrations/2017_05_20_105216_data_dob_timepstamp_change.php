<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataDobTimepstampChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('data', function (Blueprint $table) {
    	
    		// change dob type
    		$table->dateTime('dob')->nullable()->change();
    	
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('data', function (Blueprint $table) {
    	
    		// rollback dob type
    		$table->date('dob')->nullable()->change();
    	});
    }
}
