<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
        	$table->increments ( 'id' );
        	$table->string ( 'firstname' );
        	$table->string ( 'surename' );
        	$table->string ( 'email' );
        	$table->integer ( 'phone' );
        	$table->string ( 'gender' );
        	$table->string ( 'dob' );
        	$table->string ( 'comment' );
        	$table->string ( 'password' );
        	$table->rememberToken ();
        	$table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
