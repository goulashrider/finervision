<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataColumnChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('data', function (Blueprint $table) {
    		// increase phone length
    		$table->string('phone', 50)->nullable()->change();
    		
    		// change comment to longtext
    		$table->longText('comment')->nullable()->change();
    		
    		// change dob type
    		$table->date('dob')->nullable()->change();
    		
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('data', function (Blueprint $table) {
    		// decrease length
    		$table->integer('phone')->change();
    		
    		// change comment to longtext
    		$table->string('comment')->change();
    		
    		// rollback dob type
    		$table->string('dob')->change();
    	});
    }
}
